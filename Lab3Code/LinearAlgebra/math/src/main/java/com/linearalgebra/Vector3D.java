// Oni Picotte 2036356

package com.linearalgebra;

public class Vector3D {
    private double x;
    private double y;
    private double z;

    public Vector3D(double x,double y,double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double magnitude() {
        return (Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2)));
    }
    public double dotProduct(Vector3D vector) {
        return (this.x * vector.x + this.y * vector.y + this.z * vector.z);
    }
    public Vector3D add(Vector3D vector) {
        Vector3D newVector = new Vector3D(this.x + vector.x, this.y + vector.y, this.z + vector.z);
        return newVector;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
}
