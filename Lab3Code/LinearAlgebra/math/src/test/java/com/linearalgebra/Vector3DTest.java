// Oni Picotte 2036356

package com.linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3DTest {
    final double TOLERANCE = .0000001;

    @Test
    public void testGetter() {
        Vector3D v1 = new Vector3D(4, 5, 6);
        assertEquals(4, v1.getX(), TOLERANCE);
        assertEquals(5, v1.getY(), TOLERANCE);
        assertEquals(6, v1.getZ(), TOLERANCE);
    }
    @Test
    public void testMagnitude() {
        Vector3D v = new Vector3D(4, 5, 6);
        assertEquals(8.774964387, v.magnitude(), TOLERANCE);
    }
    @Test
    public void testDotProduct() {
        Vector3D v1 = new Vector3D(4, 5, 6);
        Vector3D v2 = new Vector3D(7, 8, 9);
        assertEquals(122, v1.dotProduct(v2), TOLERANCE);
    }
    @Test
    public void testAdd() {
        Vector3D v1 = new Vector3D(4, 5, 6);
        Vector3D v2 = new Vector3D(7, 8, 9);
        Vector3D v3 = v1.add(v2);
        assertEquals(11, v3.getX(), TOLERANCE);
        assertEquals(13, v3.getY(), TOLERANCE);
        assertEquals(15, v3.getZ(), TOLERANCE);
    }
}
